var express 	        = 	require( 'express' ),
	festiveApp	        = 	express( ),
    bodyParser 		    = 	require( 'body-parser' ),
    appUtils            =   require( './util/ApplicationUtils' ),
    mongoose            =   require( 'mongoose' ),
    comboController     = require( './server/controller/ComboController'),
    categoriesController = require( './server/controller/CategoriesController'),
    categoryController  = require( './server/controller/CategoryController'),
    orderController     = require( './server/controller/OrderController'),
    userController      = require ( './server/controller/UserController'),
    AuthenticationFilter = require( './server/filter/AuthenticationFilter'),
    promoCodeController = require( './server/controller/PromoController' );


console.log('Connecting to DB' );
mongoose.connect('mongodb://127.0.0.1:27017/festiveApp');
console.log('Connection Opened' );

festiveApp.use( bodyParser.json() );

festiveApp.use('*', function( req, res, next ) {

	var messageString = appUtils.getUserAddress( req ) + " -> " + appUtils.getRequestMethod( req ) + " - > " + appUtils.getRequestUrl( req );

    console.log( messageString );
	next();
});

festiveApp.use('/api/auth/*', AuthenticationFilter );


festiveApp.get(['/', '/category/:category', '/CheckOut'], function(req, res, next ) {
	res.sendFile( __dirname + '/client/' + 'index.html' );
});

festiveApp.get('/maiBaap', function( req, res,next ) {
    res.sendFile( __dirname + '/client/' + 'admin.html')
});


festiveApp.get('/resources/*', function( req, res, next ) {
    res.sendFile( __dirname + '/client/' + req.originalUrl );
});

/* Request Mapping */
festiveApp.get('/api/combos', function( req, res, next ) {
    comboController.getAllCombos( req, res, next );
});

festiveApp.post( '/api/combos', function( req, res, next ) {
    comboController.addNewCombo( req, res, next );
});

festiveApp.put( '/api/combo/:id', function( req, res, next ){
    comboController.updateCombo( req, res, next );
});

festiveApp.delete( '/api/combo/:id', function( req, res, next ){
    comboController.deleteCombo( req, res, next );
});

festiveApp.get( '/api/categories', function( req, res, next ) {
    categoriesController.getAllCategories( req, res, next );
});

festiveApp.post( '/api/categories', function( req, res,next ) {
    categoryController.addNewCategory( req, res, next );
});

festiveApp.get('/api/category/:slug', function(req,res, next){
    categoryController.getAllItemsInCategory(req, res, next );
});

festiveApp.put('/api/category/:slug', function( req, res, next ){
    categoryController.updateCategory( req, res, next );
});

festiveApp.delete('/api/category/:slug', function( req, res, next ){
    categoryController.deleteCategory( req, res, next );
});


festiveApp.post( '/api/orders', function( req, res,next ) {
    orderController.addNewOrder( req,res, next );
});

festiveApp.get( '/api/auth/user/orders', function( req, res, next ){
    orderController.getOrdersForUser( req, res, next );
});

festiveApp.get( '/api/admin/orders', function( req, res, next ) {
    orderController.getAllOrders( req, res, next );
});

festiveApp.get( '/api/admin/orders/today', function( req, res, next ) {
    orderController.getTodaysOrders( req, res, next );
});

festiveApp.put( '/api/admin/order/:id', function( req, res, next ) {
    orderController.setOrderToBeDelivered( req, res, next );
});

festiveApp.get( '/api/promo/:code', function( req, res, next ) {
    promoCodeController.getPromoCode( req, res, next );
});

festiveApp.post( '/api/user', function( req, res, next ){
    userController.isCredentialValid( req, res, next );
});

festiveApp.post( '/api/users', function( req, res, next ){
    userController.addNewUser( req, res, next );
});

festiveApp.listen( 80, function(){
	console.log( 'Magic Happens At 3000.' );
});
