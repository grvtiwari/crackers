var appConstants = {
    someError : 'someError',
    someErrorMsg : 'Some Error Occurred. Please try after some time.',

    invalidCredential : 'invalidCredential',
    invalidCredentialMsg : 'Invalid Credentials',

    userExists : 'userExists',
    userExistsMsg : 'Email is already registered',

    successMessage : 'Ok',

    jwtSecret : '#@!TyooooooooooooohaaaaaaaaaaaaaaarWale!@#',

    inValidPromoCode : 'Promo Code is Invalid'
};

module.exports = appConstants;