var mongoose = require( 'mongoose' );

var CategorySchema = new mongoose.Schema({
    name : String,
    price : Number,
    originalPrice : Number,

    imgUrl : String,
    description : String
});

var getModelFor = function( collectionName ){
    return mongoose.model( collectionName, CategorySchema, collectionName );
};

exports.getModelFor = getModelFor;