var mongoose = require( 'mongoose' );

var comboSchema = new mongoose.Schema({
    name : String,
    price : Number,
    imgUrl : String,
    description : String
});

var Combo = mongoose.model('Combo', comboSchema, 'Combo');

module.exports = Combo;