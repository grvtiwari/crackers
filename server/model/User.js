var mongoose = require( 'mongoose' );

var UserSchema = new mongoose.Schema({
    name : String,
    email : { type : String, required : true, unique : true },
    phone : String,
    password : String,
    address : String,
    registration_date : {
        type : Date,
        default : Date.now
    }
});

var User = mongoose.model( 'Users', UserSchema, 'Users' );

module.exports = User;