var mongoose = require( 'mongoose' );

var categoriesSchema = new mongoose.Schema({
    name : String,
    imgUrl : String,
    slug : String
});

var Categories = mongoose.model( 'Categories', categoriesSchema, 'Categories');

module.exports = Categories;