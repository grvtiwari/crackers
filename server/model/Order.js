var mongoose = require( 'mongoose' );

var CounterSchema = new mongoose.Schema({
    _id: {type: String, required: true},
    seq: { type: Number, default: 1000 }
});

var Counter = mongoose.model( 'Counter', CounterSchema, 'Counter' );

var ItemSchema = new mongoose.Schema({
    _id : false,
    name : String,
    qty : Number,
    price : Number,
    total_price : Number
});

var ComboSchema = new mongoose.Schema({
    _id : false,
    name : String,
    qty : Number,
    price : Number,
    total_price : Number
});

var CategorySchema = new mongoose.Schema({
    _id : false,
    name : String,
    category_price : Number,
    items : [ ItemSchema ]
});

var OrderSchema = new mongoose.Schema({
    order_id : { type : Number },
    cart : {
        combos : [ComboSchema],
        categories : [CategorySchema]
    },
    total_items : { type : Number, required : true },
    total_price : { type : Number, required : true },
    user : {
        name : { type : String, required: true },
        email : { type : String, required: true },
        phone : { type : String }
    },
    recipient : {
        name : { type : String, required: true },
        email : { type : String, required: true },
        phone : { type : String, required: true },
        address : { type : String, required: true }
    },
    date : {
        type : Date, default : Date.now
    },
    delivered : {
        type : Boolean, default : false
    },
    promo : {
        code : String,
        discount : Number,
        net : Number
    }

});

OrderSchema.pre( 'save', function( next ) {
    var doc = this;

    Counter.findByIdAndUpdate({_id: 'OrderId'}, {$inc: { seq: 1} }, function(error, counter)   {

        if(error)
            return next(error);
        doc.order_id = counter.seq;

        next();
    });
});

var Order = mongoose.model( 'Orders', OrderSchema, 'Orders' );

module.exports = Order;