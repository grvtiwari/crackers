var mongoose = require( 'mongoose' );

var PromoSchema = new mongoose.Schema({
    code : { type : String, required : true },
    discount : { type : Number, required : true }
});

var Promo = mongoose.model( 'Promo', PromoSchema, 'Promo' );

module.exports = Promo;;

