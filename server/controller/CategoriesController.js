var CategoriesService = require( '../service/CategoriesService' );

var getAllCategories = function( req, res, next ) {
    CategoriesService.getAllCategories( function( err, result ){
        if( err ){
            res.status( 500).json( result );
        } else {
            res.status( 200 ).json( result );
        }
    });
};

exports.getAllCategories = getAllCategories;