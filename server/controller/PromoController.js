var PromoCodeService = require( '../service/PromoService' );

var getPromoCode = function( req, res, next ) {
    var code = req.params.code;

    PromoCodeService.getPromoCode( code, function( err, result ) {
        if( err ) {
            res.status( 500).json( result );
        } else {
            res.status( 200 ).json( result );
        }
    });
};

exports.getPromoCode = getPromoCode;