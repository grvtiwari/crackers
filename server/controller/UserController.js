var UserService = require( '../service/UserService' );

var isCredentialValid = function( req, res, next ) {
    var userInfo = {
        email : req.body.email,
        password : req.body.password
    };

    UserService.isCredentialValid( userInfo, function( err, result ){
        if( err ){
            res.status( 500).json( result );
        } else {
            res.status( 200).json( result );
        }
    });
};

var addNewUser = function( req, res, next ) {
    var userInfo = {
        name : req.body.name,
        email : req.body.email,
        phone : req.body.contact,
        password : req.body.password
    };

    UserService.addNewUser( userInfo, function( err, result ){
        if( err ){
            res.status( 500).json( result );
        } else {
            res.status( 200).json( result );
        }
    });
};

exports.isCredentialValid = isCredentialValid;
exports.addNewUser = addNewUser;