var ComboService = require( '../service/ComboService' );

var getAllCombos = function( req, res, next ) {
    ComboService.getAllCombos( function( err, result ) {
        if( err ) {
            res.status( 500).json( result );
        } else {
            res.status( 200).json( result );
        }
    });
};

var addNewCombo = function( req, res, next ) {

    var newCombo = {
        name : req.body.name,
        price : req.body.price,
        imgUrl : req.body.imgUrl,
        description : req.body.description
    };

    ComboService.addNewCombo( newCombo, function( err, result ) {
        if( err ) {
            res.status( 500).json( result );
        } else {
            res.status( 200 ).json( result );
        }
    });
};

var updateCombo = function( req, res, next ){
    var id = req.params.id;

    var newCombo = {
        name : req.body.name,
        price : req.body.price,
        imgUrl : req.body.imgUrl,
        description : req.body.description
    };

    ComboService.updateCombo( id, newCombo, function(err, result ){
        if( err ) {
            res.status( 500).json( result );
        } else {
            res.status( 200 ).json( result );
        }
    });
};

var deleteCombo = function( req, res, next ) {
    var id = req.params.id;

    ComboService.deleteCombo( id, function( err, result ) {
        if( err ) {
            res.status( 500).json( result );
        } else {
            res.status( 200 ).json( result );
        }
    })
};

exports.getAllCombos = getAllCombos;
exports.addNewCombo = addNewCombo;
exports.updateCombo = updateCombo;
exports.deleteCombo = deleteCombo;