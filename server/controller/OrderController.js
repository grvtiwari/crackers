var OrderService = require( '../service/OrderService' );

var addNewOrder = function( req, res, next ) {
    var userInfo = req.user;
    var cartInfo = {
        cart : req.body.cart,
        person : req.body.person,
        totalItems : req.body.cart.itemsCount,
        totalPrice : req.body.cart.totalPrice
    };

    if( req.user == null ) {
        userInfo = {
            name : cartInfo.person.name,
            email : cartInfo.person.email
        };
    }

    OrderService.addNewOrder( userInfo, cartInfo, function( err, result ) {
        if( err ){
            res.status( 500).json( result );
        } else {
            res.status( 200).json( result );
        }
    });
};

var getAllOrders = function( req, res, next ) {
    OrderService.getAllOrders( function( err, result ){
        if( err ){
            res.status( 500).json( result );
        } else {
            res.status( 200).json( result );
        }
    });
};

var getOrdersForUser = function( req, res,next ){
    var userEmail = req.user.email;

    OrderService.getOrdersForUser( userEmail, function(err, result){
        if( err ){
            res.status( 500).json( result );
        } else {
            res.status( 200).json( result );
        }
    });
};

var getTodaysOrders = function ( req, res, next ){
    OrderService.getTodaysOrders( function( err, result ){
        if( err ){
            res.status( 500).json( result );
        } else {
            res.status( 200).json( result );
        }
    });
};

var setOrderToBeDelivered = function( req, res, next ) {
    var id = req.params.id;

    OrderService.setOrderToBeDelivered( id, function( err, result ){
        if( err ){
            res.status( 500).json( result );
        } else {
            res.status( 200).json( result );
        }
    });
};

exports.addNewOrder = addNewOrder;
exports.getAllOrders = getAllOrders;
exports.getOrdersForUser = getOrdersForUser;
exports.getTodaysOrders = getTodaysOrders;
exports.setOrderToBeDelivered = setOrderToBeDelivered;