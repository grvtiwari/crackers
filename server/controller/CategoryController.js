var CategoryService = require( '../service/CategoryService');

var addNewCategory = function( req, res, next ) {
    var newCategoryObject = {
        name : req.body.name,
        imgUrl : req.body.imgUrl,
        slug : req.body.slug,

        categoryItems : req.body.items
    };

    console.log( newCategoryObject );

    CategoryService.addNewCategory( newCategoryObject, function( err, result ){
        if( err ) {
            res.status( 500).json( result );
        } else {
            res.status( 200).json( result );
        }
    });
};

var getAllItemsInCategory = function(req, res, next ){
    var slug = req.params.slug;

    CategoryService.getAllItemsInCategory( slug, function( err, result ){
        if( err ) {
            res.status( 500).json( result );
        } else {
            res.status( 200).json( result );
        }
    })
};

var updateCategory = function(req, res, next ){
    var category = {
        slug : req.params.slug,
        categoryItems : req.body.items
    };

    CategoryService.updateCategory( category, function( err, result ){
        if( err ) {
            res.status( 500).json( result );
        } else {
            res.status( 200).json( result );
        }
    });
};

var deleteCategory = function( req, res, next ){
    var slug = req.params.slug;

    CategoryService.deleteCategory( slug, function( err, result ){
        if( err ) {
            res.status( 500).json( result );
        } else {
            res.status( 200).json( result );
        }
    });
};

exports.addNewCategory  = addNewCategory ;
exports.getAllItemsInCategory = getAllItemsInCategory;
exports.updateCategory = updateCategory;
exports.deleteCategory = deleteCategory;