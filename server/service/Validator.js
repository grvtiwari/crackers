/* This module would act as Validtor Service.
 * Developer is responsible to make sure that method are named in such a way that following conditions are met.
 * True => Validation Failed.
 * False => Validation Passed.
 * Methods would return an object like.
 * {
 *      result : result,
 *      message : errorMessage
 * }
 */

var RegExProvider = {
    name: /^[a-zA-Z ]{1,}$/,
    email: /^[_A-Za-z0-9-\+]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,})$/,
    contactNumber : /^[1-9][0-9]{9,10}$/,
    number : /^[0-9]{1,}$/,
    address : /[a-zA-Z0-9 ,-\/]{1,}/
};

var isFieldNotValid = function (value, isMandatory, mandatoryMessage, regEx, inValidMessage){

    if (isMandatory && (value === '' || null == value)) {
        return {
            result : true,
            message : mandatoryMessage
        };
    } else if (value === '' || value == null) {
        return {
            result : false
        };
    }

    if (!regEx) {
        return {
            result : false
        };
    }

    var result = !regEx.test(value);

    if (result) {
        return {
            result : result,
            message : inValidMessage
        };
    } else {
        return {
            result : result
        };
    }
};

var mandatoryMessage = 'Field is empty.'

var isNameNotValid = function (name, isMandatory) {
    return isFieldNotValid(name, isMandatory, mandatoryMessage, RegExProvider.name, "Invalid Name.");
};

var isEmailNotValid = function (email, isMandatory) {
    return isFieldNotValid(email, isMandatory, mandatoryMessage, RegExProvider.email, "Invalid E-mail Address.");
};

var isPasswordNotValid = function (credential, isMandatory) {
    return isFieldNotValid(credential, isMandatory, mandatoryMessage);
};

var isContactNotValid = function (contact, isMandatory) {
    return isFieldNotValid(contact, isMandatory, mandatoryMessage, RegExProvider.contactNumber, "Invalid Phone Number.");
};

var isAddressNotValid = function( address, isMandatory ) {
    return isFieldNotValid( address, isMandatory, mandatoryMessage, RegExProvider.address, "Invalid Address." );
}


exports.isNameNotValid = isNameNotValid;

exports.isEmailNotValid = isEmailNotValid;
exports.isPasswordNotValid = isPasswordNotValid;

exports.isContactNotValid = isContactNotValid;
exports.isAddressNotValid = isAddressNotValid;