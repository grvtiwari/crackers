var PromoDBI = require( '../dao/PromoCodeDBI' );
var ApplicationConstants = require( '../constants/ApplicationConstants' );

var getPromoCode = function( code, callback ) {
    PromoDBI.getPromoCode( code, function( err, result ){
        if( err ) {
            console.log('PromoService | getPromoCode | ' + JSON.stringify( err ) );
            callback( ApplicationConstants.someError, { result : null, msg : ApplicationConstants.someErrorMsg });
        } else {
            if( result == null ) {
                callback( ApplicationConstants.inValidPromoCode, { result : null, msg : ApplicationConstants.inValidPromoCode});
            } else {
                callback( null, { result : result, msg : ApplicationConstants.successMessage, token : result });
            }
        }
    })
};

exports.getPromoCode = getPromoCode;