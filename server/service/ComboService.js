var ComboDBI = require( '../dao/ComboDBI' );
var ApplicationConstants = require( '../constants/ApplicationConstants' );

var getAllCombos = function( callback ) {
    ComboDBI.getAllCombos( function( err, result ) {
        if( err ) {
            callback( ApplicationConstants.someError, {result: null, msg:ApplicationConstants.someErrorMsg});
        } else {
            callback( null, { result : result, msg : ApplicationConstants.successMessage });
        }
    });
};

var addNewCombo = function( newCombo, callback ) {
    ComboDBI.addNewCombo( newCombo, function( err, result ) {
        if( err ) {
            callback( ApplicationConstants.someError, { result : null, msg : ApplicationConstants.someErrorMsg });
        } else {
            callback( null, { result : result, msg : ApplicationConstants.successMessage });
        }
    });
};


var updateCombo = function( id, newCombo, callback ) {
    ComboDBI.updateCombo( id, newCombo, function( err, result ){
        if( err ) {
            console.log( 'ComboService | updateCombo | Error ' + JSON.stringify( err ));
            callback( ApplicationConstants.someError, { result : null, msg : ApplicationConstants.someErrorMsg });
        } else {

            if( result == 0 )
                callback( ApplicationConstants.someError, { result : null, msg : ApplicationConstants.someErrorMsg });
            else
                callback( null, { result : result, msg : ApplicationConstants.successMessage });
        }
    });
};

var deleteCombo = function (id, callback ) {
    ComboDBI.deleteCombo( id, function( err, result ) {
        if( err ) {
            console.log( 'ComboService | deleteCombo | Error ' + JSON.stringify( err ));
            callback( ApplicationConstants.someError, { result : null, msg : ApplicationConstants.someErrorMsg });
        } else {
            if( result.n == 0 )
                callback( ApplicationConstants.someError, { result : null, msg : ApplicationConstants.someErrorMsg });
            else
                callback( null, { result : result, msg : ApplicationConstants.successMessage });
        }
    });
};

exports.getAllCombos = getAllCombos;
exports.addNewCombo = addNewCombo;
exports.updateCombo = updateCombo;
exports.deleteCombo = deleteCombo;