var OrderDBI = require( '../dao/OrderDBI' );
var ApplicationConstants = require( '../constants/ApplicationConstants' );

var Validator = require( './Validator' );

var filterObjectInformation = function( orderObject ) {
    var newObjectInfo  = {
        cart : orderObject.cart,
        order_id : orderObject.order_id,
        recipient : orderObject.recipient,
        total_items : orderObject.total_items,
        total_price : orderObject.total_price,
        user : orderObject.user
    };


    return newObjectInfo;
};
var addNewOrder = function(userInfo, cartInfo, callback ) {
    var err = {};
    var errMsg = {};

    var hasAnyValidationFailed = false;

    var responseFromValidatorForName = Validator.isNameNotValid(  cartInfo.person.name, true  );
    var responseFromValidatorForEmail = Validator.isEmailNotValid( cartInfo.person.email, true );

    var responseFromValidatorForContact = Validator.isContactNotValid( cartInfo.person.contact, true );
    var responseFromValidatorForAddress = Validator.isAddressNotValid( cartInfo.person.address, true );

    if( responseFromValidatorForName.result ){
        hasAnyValidationFailed = true;

        err['name'] = true;
        errMsg['name'] = responseFromValidatorForName.message;
    }

    if( responseFromValidatorForEmail.result ){
        hasAnyValidationFailed = true;

        err['email'] = true;
        errMsg['email'] = responseFromValidatorForEmail.message;
    }

    if( responseFromValidatorForContact.result ){
        hasAnyValidationFailed = true;

        err['contact'] = true;
        errMsg['contact'] = responseFromValidatorForContact.message;
    }

    if( responseFromValidatorForAddress.result ){
        hasAnyValidationFailed = true;

        err['address'] = true;
        errMsg['address'] = responseFromValidatorForAddress.message;
    }

    if( hasAnyValidationFailed ) {
        callback( ApplicationConstants.someError, { result : null, err : err, errMsg : errMsg, msg : ApplicationConstants.someErrorMsg });
    } else {

        OrderDBI.addNewOrder( userInfo, cartInfo, function( err, result ){
            if( err ) {
                console.log('OrderService | addNewOrder ' + JSON.stringify( err ) );
                callback( ApplicationConstants.someError, { result : null, msg : ApplicationConstants.someErrorMsg });
            } else {
                callback( null , { result : filterObjectInformation(result), msg : ApplicationConstants.successMessage });
            }
        });
    }
};

var getAllOrders = function( callback ) {
    OrderDBI.getAllOrders( function( err, result ) {
        if( err ) {
            console.log('OrderService | getAllOrders ' + JSON.stringify( err ) );
            callback( ApplicationConstants.someError, { result : null, msg : ApplicationConstants.someErrorMsg });
        } else {
            callback( null , { result : result, msg : ApplicationConstants.successMessage });
        }
    }) ;
};

var getOrdersForUser = function( userEmail, callback ){
    OrderDBI.getOrdersForUser( userEmail, function( err, result ){
        if( err ){
            console.log( 'OrderService | getOrdersForUser ' + JSON.stringify( err ));
            callback( ApplicationConstants.someError, { result : null, msg : ApplicationConstants.someErrorMsg });
        } else {
            callback( null , { result : result, msg : ApplicationConstants.successMessage });
        }
    });
};

var getTodaysOrders = function( callback ) {
    OrderDBI.getTodaysOrders( function( err, result ){
        if( err ){
            console.log( 'OrderService | getTodaysOrders ' + JSON.stringify( err ));
            callback( ApplicationConstants.someError, { result : null, msg : ApplicationConstants.someErrorMsg });
        } else {
            callback( null , { result : result, msg : ApplicationConstants.successMessage });
        }
    })  ;
};

var setOrderToBeDelivered = function( id, callback ){
    OrderDBI.setOrderToBeDelivered( id, function( err, result ){
        if( err ){
            console.log( 'OrderService | setOrderToBeDelivered ' + JSON.stringify( err ));
            callback( ApplicationConstants.someError, { result : null, msg : ApplicationConstants.someErrorMsg });
        } else {
            callback( null , { result : result, msg : ApplicationConstants.successMessage });
        }
    });
};

exports.addNewOrder = addNewOrder;
exports.getAllOrders = getAllOrders;
exports.getOrdersForUser = getOrdersForUser;
exports.getTodaysOrders = getTodaysOrders;
exports.setOrderToBeDelivered = setOrderToBeDelivered;