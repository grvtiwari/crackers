var UserDBI = require( '../dao/UserDBI' );
var ApplicationConstants = require( '../constants/ApplicationConstants' );
var ApplicationUtils = require( '../util/ApplicationUtils' );

var jwt = require( 'jwt-simple' );

var generateToken = function (user) {
        user.generate = new Date().getTime();
        var token =  jwt.encode( user, ApplicationConstants.jwtSecret );
        delete user.generate;

        return token;
};

var isCredentialValid = function( userInfo, callback ){
    userInfo.password = ApplicationUtils.getEncryptedPassword( userInfo.password );

    UserDBI.isCredentialValid( userInfo, function( err, result ) {
        if( err ) {
            console.log('UserService | isCredentialValid | ' + JSON.stringify( err ) );
            callback( ApplicationConstants.someError, { result : null, msg : ApplicationConstants.someErrorMsg });
        } else {
            if( result == null ) {
                callback( ApplicationConstants.invalidCredential, { result : null, msg : ApplicationConstants.invalidCredentialMsg });
            } else {
                callback( null, { result : result, msg : ApplicationConstants.successMessage, token : generateToken( result ) });
            }
        }
    })
};

var addNewUser = function( userInfo, callback ) {
    userInfo.password = ApplicationUtils.getEncryptedPassword( userInfo.password );

    UserDBI.addNewUser( userInfo, function( err, result ){
        if( err ) {
            console.log('UserService | addNewUser ' + JSON.stringify( err ) );

            if( err.code == 11000 || err.code == 11001 ) {
                callback( ApplicationConstants.userExists, { result : null, msg : ApplicationConstants.userExistsMsg });
            } else
                callback( ApplicationConstants.someError, { result : null, msg : ApplicationConstants.someErrorMsg });
        } else {
            callback( null , { result : result, msg : ApplicationConstants.successMessage });
        }
    })
};

exports.isCredentialValid = isCredentialValid;
exports.addNewUser = addNewUser;
