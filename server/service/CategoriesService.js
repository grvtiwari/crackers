var CategoriesDBI = require( '../dao/CategoriesDBI' );
var ApplicationConstants = require( '../constants/ApplicationConstants' );
var CategoryDBI = require( '../dao/CategoryDBI' );

var async = require( 'async' );

var getAllCategories = function( callback ) {
    CategoriesDBI.getAllCategories( function( err, result ){
        if( err ) {
            console.log( 'CategoriesService | getAllCategories ' + JSON.stringify( err ));
            callback( ApplicationConstants.someError, {result : null, msg : ApplicationConstants.someErrorMsg });
        } else {
            callback( null, { result : result, msg : ApplicationConstants.successMessage });
        }
    });
};

var addNewCategoryToCategoryList = function( categoryObject, callback ) {
    CategoriesDBI.addNewCategoryToCategoryList( categoryObject, function( err, result ) {
        if( err ) {
            console.log( 'CategoriesService | addNewCategoryToCategoryList ' + JSON.stringify( err ));
            callback( ApplicationConstants.someError, {result : null, msg : ApplicationConstants.someErrorMsg })
        } else {
            callback( null, { result : result, msg : ApplicationConstants.successMessage });
        }
    })
};

var deleteCategory = function( slug, callback ) {
    CategoriesDBI.deleteCategory( slug, function( err, result ){
        if( err ) {
            console.log( 'CategoriesService | deleteCategory ' + JSON.stringify( err ));
            callback( ApplicationConstants.someError, {result : null, msg : ApplicationConstants.someErrorMsg })
        } else {
            callback( null, { result : result, msg : ApplicationConstants.successMessage });
        }
    });
};

exports.getAllCategories = getAllCategories;
exports.addNewCategoryToCategoryList  = addNewCategoryToCategoryList;
exports.deleteCategory = deleteCategory;