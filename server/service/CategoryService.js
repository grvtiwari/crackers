var CategoryDBI = require( '../dao/CategoryDBI' );
var CategoriesDBi = require( '../dao/CategoriesDBI' );

var ApplicationConstants = require( '../constants/ApplicationConstants' );

var async = require( 'async' );

var addNewCategory = function( categoryObject, callback ) {

    async.series({
        insertIntoCategories : async.apply( CategoriesDBi.addNewCategoryToCategoryList, categoryObject ),
        createNewCategoryCollection : async.apply( CategoryDBI.createNewCategory, categoryObject )
    }, function( err, result ) {
        if( err ) {
            console.log( 'CategoryService | addNewCategory ' + JSON.stringify( err ));
            callback( ApplicationConstants.someError, {result : null, msg : ApplicationConstants.someErrorMsg });
        } else {
            callback( null, { result : result, msg : ApplicationConstants.successMessage });
        }
    });
};

var getAllItemsInCategory = function( slug, callback ) {
    CategoryDBI.getAllItemsInCategory( slug, function( err, result ){
        if( err ) {
            console.log( 'CategoryService | getAllItemsInCategory ' + JSON.stringify( err ));
            callback( ApplicationConstants.someError, {result : null, msg : ApplicationConstants.someErrorMsg });
        } else {
            callback( null, { result : result, msg : ApplicationConstants.successMessage });
        }
    });
};

var updateCategory = function( categoryObject, callback ) {
    var slug = categoryObject.slug;

    async.series({
        emptyCollection : async.apply( CategoryDBI.emptyCollection, slug ),
        insertIn : async.apply( CategoryDBI.createNewCategory, categoryObject )
    }, function( err, result ){
        if( err ) {
            console.log( 'CategoryService | updateCategory ' + JSON.stringify( err ));
            callback( ApplicationConstants.someError, {result : null, msg : ApplicationConstants.someErrorMsg });
        } else {
            callback( null, { result : result, msg : ApplicationConstants.successMessage });
        }
    });
};

var deleteCategory = function( slug, callback ){
    async.series({
        deleteFromCategories : async.apply( CategoriesDBi.deleteCategory, slug ),
        emptyCollection : async.apply( CategoryDBI.emptyCollection, slug )
    }, function( err, result ){
        if( err ) {
            console.log( 'CategoryService | deleteCategory ' + JSON.stringify( err ));
            callback( ApplicationConstants.someError, {result : null, msg : ApplicationConstants.someErrorMsg });
        } else {
            callback( null, { result : result, msg : ApplicationConstants.successMessage });
        }
    })
}

exports.addNewCategory  = addNewCategory;
exports.getAllItemsInCategory = getAllItemsInCategory;
exports.updateCategory = updateCategory;
exports.deleteCategory = deleteCategory;