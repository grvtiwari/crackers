var Categories = require( '../model/Categories' );
var DBIUtil = require( './DBIUtils' );

var getAllCategories = function( callback ) {
    Categories.find({}, function( err, result ){
        callback( err, result );
    });
};

var addNewCategoryToCategoryList = function( categoryObject, callback ) {

    var Category = new Categories({
        name : categoryObject.name,
        slug : categoryObject.slug,
        imgUrl : categoryObject.imgUrl
    });

    Category.save( function( err, result ){
        callback( err, result );
    });
};

var deleteCategory = function( slug, callback ){
    var query = { slug : slug };

    Categories.remove( query, function( err, result ){
        callback( err, result );
    });
};

exports.getAllCategories = getAllCategories;
exports.addNewCategoryToCategoryList  = addNewCategoryToCategoryList;
exports.deleteCategory = deleteCategory;