var Combo = require( '../model/ComboModel' );
var DBIUtil = require( './DBIUtils' );

var getAllCombos = function(callback) {
        Combo.find({}, function(err, result ) {
            callback(err, result);
        });
};

var addNewCombo = function( newCombo, callback ) {
    var combo = new Combo({
        name : newCombo.name,
        description : newCombo.description,
        imgUrl : newCombo.imgUrl,
        price : newCombo.price
    });

    combo.save( function(err, result ) {
        callback( err, result );
    });
};

var updateCombo = function( id, newCombo, callback ) {
    var query = { '_id' : DBIUtil.getObjectIdFromString( id ) };

    var update = {$set : { name : newCombo.name, description: newCombo.description, imgUrl : newCombo.imgUrl, price: newCombo.price }};

    Combo.update( query, update, function( err, updateObject ) {
        callback( err, updateObject.nModified );
    });
};

var deleteCombo = function( id, callback ) {
    var query = {
        '_id' : DBIUtil.getObjectIdFromString( id )
    };

    Combo.remove( query, function( err, result ){
        callback( null, result );
    });
};

exports.getAllCombos = getAllCombos;
exports.addNewCombo = addNewCombo;
exports.updateCombo  = updateCombo;
exports.deleteCombo  = deleteCombo;