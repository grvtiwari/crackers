var OrderModel = require( '../model/Order' );
var DBIUtil = require( './DBIUtils' );

var addNewOrder = function( userInfo, cartInfo, callback ){

    var orderInfo = {
        cart : {
            combos : cartInfo.cart.combos.items,
            categories : cartInfo.cart.categories.items
        },
        total_items : cartInfo.totalItems,
        total_price : cartInfo.totalPrice,
        user : {
            name : userInfo.name,
            email : userInfo.email,
            phone : userInfo.contact
        },
        recipient : {
            name : cartInfo.person.name,
            email : cartInfo.person.email,
            phone : cartInfo.person.contact,
            address : cartInfo.person.address
        }
    };

    if( typeof cartInfo.cart.promo != 'undefined' && typeof  cartInfo.cart.promo.code != 'undefined' ) {
        orderInfo.promo = {
            code : cartInfo.cart.promo.code,
            discount : cartInfo.cart.promo.discount,
            net : cartInfo.cart.promo.net
        };
    };

    var Order = new OrderModel( orderInfo );

    Order.save( function(err, doc ) {
        callback( err, doc );
    });
};

var getAllOrders = function( callback ) {
    OrderModel.find({}, function(err, result ){
        callback( err, result );
    });
};

var getOrdersForUser = function( userEmail, callback ){
    var query = {
        'user.email' : userEmail
    };

    OrderModel.find( query, function( err, result ){
        callback( err, result );
    });
};

var getTodaysDate = function () {
    var date = new Date();

    var day = date.getDay();
    var month = date.getMonth();
    var fullYear = date.getFullYear();

    return new Date(fullYear, month, day );
};

var getTodaysOrders = function (callback) {
    var query = {
        date : {
            '$gt' : getTodaysDate()
        }
    };

    OrderModel.find( query, function( err, result ){
        callback( err, result );
    });
};

var setOrderToBeDelivered = function (id, callback ) {

    try{
        id = parseInt( id );
    }catch( err ) {
        callback( 'Error' );
    }

    var query = { 'order_id' : id };

    console.log( query );
    console.log( updateQuery );

    var updateQuery = {
        $set : {'delivered' : true }
    };

    console.log( updateQuery );

    OrderModel.update(query, updateQuery, function ( err, result ){
        console.log( result );
        callback( err, result );
    });
};

exports.addNewOrder = addNewOrder;
exports.getAllOrders = getAllOrders;
exports.getOrdersForUser = getOrdersForUser;
exports.getTodaysOrders  = getTodaysOrders ;
exports.setOrderToBeDelivered = setOrderToBeDelivered;