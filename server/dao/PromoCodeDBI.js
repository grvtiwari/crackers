var Promo = require( '../model/Promo' );

var getPromoCode = function( promoCode, callback ) {
    var query = {
        code : promoCode
    };

    Promo.findOne( query, function( err, result ) {
        callback(err, result );
    });
};

exports.getPromoCode = getPromoCode;