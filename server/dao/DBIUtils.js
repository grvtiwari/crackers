var mongoose = require( 'mongoose' );

var getObjectIdFromString = function( id ) {
    return mongoose.Types.ObjectId( id );
};

exports.getObjectIdFromString = getObjectIdFromString;