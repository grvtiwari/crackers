var UserModel = require( '../model/User' );

var isCredentialValid = function( userInfo, callback ) {
    var query = {
        email : new RegExp( '' +   userInfo.email, 'i' ),
        password : userInfo.password
    };

    UserModel.findOne( query, function( err, result ){
        var userInfo = {};

        if( null!= result && typeof result._id != 'undefined' ){
            userInfo = {
                name : result.name,
                email : result.email
            };

            result = userInfo;
        }
        console.log( result );
        callback( err, result );
    });
};

var addNewUser = function( userInfo, callback ) {
    var User = new UserModel({
        name : userInfo.name,
        email : userInfo.email,
        phone : userInfo.contact,
        password : userInfo.password
    });

    User.save( function( err, result ){
        var userInfo = {};
        if( !err ) {

            userInfo = {
                name : result.name,
                email : result.email
            };

            result = userInfo;
        }
        callback( err, result );
    })
};

exports.isCredentialValid = isCredentialValid;
exports.addNewUser = addNewUser;
