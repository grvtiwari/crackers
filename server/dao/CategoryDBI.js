var CategoryModel = require( '../model/Category' );
var DBIUtil = require( './UserDBI' );

var createNewCategory = function( categoryObject, callback ) {

    var NewCollectionModel = CategoryModel.getModelFor( categoryObject.slug );

    NewCollectionModel.create( categoryObject.categoryItems, function( err, result ){
        callback( err, result );
    });

};

var emptyCollection = function ( slug, callback ) {
    var CollectionModel = CategoryModel.getModelFor( slug );

    CollectionModel.remove({}, function( err, result ){
        callback( err, result );
    });
};

var getAllItemsInCategory = function( slug, callback )  {
    var CollectionModel = CategoryModel.getModelFor( slug );

    var projection = {
        '_id' : false,
        '__v' : false,
        'originalPrice' : false
    };

    CollectionModel.find( {}, projection, function( err, result ){
        callback( err, result );
    });
};

exports.createNewCategory = createNewCategory;
exports.emptyCollection = emptyCollection;
exports.getAllItemsInCategory = getAllItemsInCategory;