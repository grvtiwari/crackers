var app = angular.module( 'AnchorScroll', [] );

app.service( 'SmoothScroll', function($interval){

    var easeInOutQuad = function(currentIteration, startValue, changeInValue, totalIterations) {
        if ((currentIteration /= totalIterations / 2) < 1) {
            return changeInValue / 2 * currentIteration * currentIteration + startValue;
        }
        return -changeInValue / 2 * ((--currentIteration) * (currentIteration - 2) - 1) + startValue;
    };

    this.totalIterations = 30;
    var self = this;

    var animateScroll = function () {
        var toScrollTo = easeInOutQuad(self.currentIteration++, self.startValue, self.changeInValue, self.totalIterations);

        document.body.scrollTop = parseInt( toScrollTo );

        if( self.currentIteration > self.totalIterations ) {
            $interval.cancel( self.stopPromise );
        }
    };

    this.scrollTo = function ( divId ) {

        var currentScroll = self.startValue = document.body.scrollTop;
        var neededScrollAt = document.getElementById( divId ).offsetTop - 44;

        self.changeInValue = neededScrollAt - currentScroll;
        self.currentIteration = 0;
        self.stopPromise = $interval( function () {
            animateScroll();
        }, 10);
    };
});