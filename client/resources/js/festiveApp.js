var app = angular.module( 'festiveApp', ['ngRoute', 'ngTouch']);

app.config(['$routeProvider', '$locationProvider', '$httpProvider', function( $routeProvider, $locationProvider,$httpProvider) {
    $routeProvider.when('/',{
        templateUrl : '/resources/pages/festiveHome.html',
        controller : 'FestiveHomeController'})
    .when('/category/:category',{
            templateUrl : '/resources/pages/category/categoryList.html',
            controller : 'CategoryListController'})
    .when( '/checkout', {
            templateUrl : '/resources/pages/checkout/checkout.html',
            controller : 'CheckOutController'
        })
    .when( '/orders', {
            templateUrl : '/resources/pages/orders/orderList.html',
            controller : 'OrderListController'
        });

    $httpProvider.interceptors.push("authInterceptor");
}]);

app.run(['$http', 'Combos', 'Categories', 'User', 'CategoryItems', '$rootScope', 'Cart', function($http, Combos, Categories, User, CategoryItems, $rootScope, Cart){
    var requestForCombos= 'api/combos';
    var requestForCategories = 'api/categories';

    User.readLoggedInUserData();

    $http.get( requestForCombos)
        .success( function( data ){Combos.setCombos(data.result)})
        .error( function( data ) {});

    $http.get( requestForCategories )
        .success( function( data ) {Categories.setCategories( data.result ) })
        .error( function( data ) {});

    $http.get( requestForCategories )
        .success(function( data ){
            Categories.setCategories( data.result );

            angular.forEach( data.result, function( categoryObject, index ) {
                var requestName = 'api/category/' + categoryObject.slug;

                $http.get( requestName )
                    .success( function(data ) {
                        CategoryItems.setItemsFor(categoryObject.slug, data.result );

                        if( index  == ( data.result.length -1 ) ) {
                            $rootScope.$broadcast( 'categoryItemsUpdated' );
                        }
                    });
            })
        });
}]);

app.factory( 'User', ['$window', '$rootScope', function( $window, $rootScope) {
    var userInfo = {};

    var braodcastEvent = function () {
        $rootScope.$broadcast('updatedUserInfo');
    };

    return {
        readLoggedInUserData: function () {
            if( angular.isDefined( $window.localStorage.user)){
                userInfo = JSON.parse( $window.localStorage.user );
                userInfo.token = $window.localStorage.token;
            }
        },
        setUserInfo: function( data ){
            userInfo = data;
            $window.localStorage.user = JSON.stringify(data);

            braodcastEvent();
        },
        setUserToken : function ( token ) {
            userInfo.token = token;
            $window.localStorage.token = token;
        },
        isUserLoggedIn: function() {
            if( angular.isDefined( userInfo.name ))
                return true;
            return false;
        },
        getUserName : function () {
            return userInfo.name;
        },
        getUserEmail : function () {
            return userInfo.email
        },
        logoutUser : function () {
            delete $window.localStorage.user;
            delete $window.localStorage.token;
            userInfo = {};

            braodcastEvent();
        }
    }
}]);

app.factory( 'AppConstants', function () {
    return {
        appName : 'Festive App',
        genericImgUrl : 'data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==',
        heading : 'We add flavours to your TYOHAARs.'
    }
});

app.factory( 'RequestCount', function () {
    var requestCount = 0;

    return {
        init : function() { requestCount = 0 },
        isZero : function () { return requestCount === 0 },
        addOne : function () { requestCount = requestCount + 1 },
        subOne : function () { requestCount = requestCount - 1 }
    };
})

app.factory('Combos', ['$rootScope', function($rootScope){
    var data = [];

    return {
        getCombos : function () { return data;},
        setCombos : function (combos){
            data = combos;
            $rootScope.$broadcast('updateScope');
        }
    }
}]);

app.factory('Categories', ['$rootScope', function ($rootScope) {
    var data = [];

    return {
        getCategories : function (){return data;},
        setCategories : function( result ) {
            data = result;
            $rootScope.$broadcast('updateScope');
        }
    }
}]);

app.factory('CategoryItems', function(){
    var data = {};

    return {
        getItemsFor : function( category ) { return data[ category ];},
        setItemsFor : function( category, result ) { data[ category ] = result;}
    };
});

app.directive('box', ['AppConstants', '$location', 'Cart', function(AppConstants, $location, Cart) {
    return {
        restrict : 'A',
        scope : {
            item : '=',
            hasPrice : '@',
            type : '@'
        },
        replace : true,
        templateUrl : 'resources/pages/boxLayout.html',
        link : function( scope, element, attrs ){
            scope.cartActive = false;
            scope.getImageUrl = function (item) {
                if( angular.isUndefined( item.imgUrl )){
                    return AppConstants.genericImgUrl;
                }
                return item.imgUrl;
            };

            scope.itemDoesNotHasImage = function( item ) {
                if( angular.isUndefined( item.imgUrl )){
                    return true;
                }
                return false;
            };

            scope.doesItemHasPrice = function (){
                return scope.hasPrice == 'true';
            };

            scope.openCategory = function ( item ) {
                $location.path( '/category/' + item.slug );
            };

            scope.activateCart = function() {
                scope.cartActive = true;
            };

            scope.isCartActive = function () {
                return scope.cartActive;
            };

            scope.getPrice = function ( item ) {
                return angular.isDefined( item.qty ) ? (item.price * item.qty ) : item.price;
            };

            scope.addToCart = function( item ){
                item.finalized = true;
                if( scope.type === 'combo' ) {

                    Cart.updateCombosInCart( scope.$parent.combos );
                } else {
                    Cart.updateCategoriesInCart( scope.$parent.categories );
                }
            };
        }
    }
}]);

app.directive('triSlider', ['AppConstants', '$location', 'Cart', function(AppConstants, $location, Cart){
    return {
        restrict : 'A',
        scope : {
            items : '=',
            hasPrice : '@',
            type : '@'
        },
        replace : true,
        templateUrl : 'resources/pages/directiveLayout.html',
        link : function( scope, element, attrs ){

            scope.currentSlide = 0;
            scope.initialMargin = 0;

            scope.getWidthInPercentageForContainer = function () {
                return ( 100 * scope.items.length ) + '%';
            };

            scope.getWidthForBox = function (){
                scope.initialMarginValue = ( 15 / scope.items.length );
                scope.initialMargin = ( 15 / scope.items.length ) + '%';
                scope.boxWidth = 70 / scope.items.length;
                return ( 70 / scope.items.length )  + '%';
            };

            scope.getMarginForBox = function ($index) {
                if( $index == 0 ){
                    if( scope.currentSlide == 0 ){
                        return scope.initialMargin;
                    }

                    return (scope.initialMarginValue - (( scope.boxWidth * scope.currentSlide ) +(scope.initialMarginValue / scope.items.length * scope.currentSlide) )) + '%';
                }

                return 0;
            };

            scope.next = function () {
                if( scope.currentSlide == (scope.items.length -1))
                    return;

                scope.currentSlide++;
            };

            scope.previous = function () {
                if( scope.currentSlide == 0 )
                    return;

                scope.currentSlide--;
            };

            scope.getImageUrl = function (item) {
                if( angular.isUndefined( item.imgUrl )){
                    return AppConstants.genericImgUrl;
                }
                return item.imgUrl;
            };

            scope.itemDoesNotHasImage = function( item ) {
                if( angular.isUndefined( item.imgUrl )){
                    return true;
                }
                return false;
            };

            scope.doesItemHasPrice = function (){
                return scope.hasPrice == 'true';
            };

            scope.openCategory = function ( item ) {
                $location.path( '/category/' + item.slug );
            };

            scope.activateCart = function(item) {
                item.cartActive = true;
            };

            scope.isCartActive = function (item) {
                return item.cartActive;
            };

            scope.getPrice = function ( item ) {
                return angular.isDefined( item.qty ) ? (item.price * item.qty ) : item.price;
            };

            scope.addToCart = function( item ){
                item.finalized = true;
                if( scope.type === 'combo' ) {

                    Cart.updateCombosInCart( scope.items );
                } else {
                    Cart.updateCategoriesInCart( scope.$parent.categories );
                }
            };
        }
    }
}]);

app.directive('customInput', ['$timeout', function($timeout){
    return {
        restrict : 'A',
        scope : {
            value : '='
        },
        replace : true,
        template : '<div class="inputContainer">' +
        '<div class="glyphContainer"><span class="glyphicon glyphicon-chevron-up" ng-click="oneMore()"></span></div>' +
        '<div class="input"><input class="form-control" ng-model="value" type="text"/></div>' +
        '<div class="glyphContainer"><span class="glyphicon glyphicon-chevron-down" ng-click="oneLess()"></span></div>' +
        '</div>',
        link : function ( scope, element, attrs ) {
            if( scope.value == '' || scope.value == null || angular.isUndefined( scope.value )){
                scope.value = 1;
            }
            scope.emitEvent = function() {
                $timeout(function () {
                    scope.$emit('updateCartInController')
                }, 100);
            };
            scope.oneLess = function(){
                if( typeof scope.value == 'undefined' )
                    scope.value = 0;

                if( scope.value > 0 ) {
                    scope.value--;

                    scope.emitEvent();
                }
            };

            scope.oneMore = function(){
                if( typeof scope.value == 'undefined' )
                    scope.value = 0;

                scope.value++;
                scope.emitEvent();
            };
        }
    }
}]);

app.service('authInterceptor', ['$rootScope', '$q', '$window', 'User', 'RequestCount', function ($rootScope, $q, $window, User, RequestCount) {
    return {
        request: function (config) {

            if( RequestCount.isZero() ) {
                var overlayDiv = angular.element( document.getElementById( 'overlay' ));
                overlayDiv.css('visibility', 'visible');
                overlayDiv.addClass('active');

                var loader = angular.element( document.getElementById( 'loader' ));
                loader.css( 'display' , 'block' );
            }

            RequestCount.addOne();

            config.headers = config.headers || {};

            if (config.url.indexOf('api/auth') !== 0)
                return config;

            if ($window.localStorage.getItem('token')) {
                config.headers.Authorization = 'Bearer ' + $window.localStorage.token;
            }



            return config;
        },
        response: function (response) {

            RequestCount.subOne();

            if( RequestCount.isZero() ) {
                var overlayDiv = angular.element( document.getElementById( 'overlay' ));
                overlayDiv.css('visibility', 'hidden');
                overlayDiv.removeClass('active');

                var loader = angular.element( document.getElementById( 'loader' ));
                loader.css( 'display' , 'none' );
            }
            return response;
        },
        responseError: function (rejection) {
            if( rejection.status === 401 && rejection.data.data === 'TE' ){
                /* Logging out user so as to avoid redirection.*/
                User.logoutUser();
            }

            RequestCount.subOne();

            if( RequestCount.isZero() ) {
                var overlayDiv = angular.element( document.getElementById( 'overlay' ));
                overlayDiv.css('visibility', 'hidden');
                overlayDiv.removeClass('active');

                var loader = angular.element( document.getElementById( 'loader' ));
                loader.css( 'display' , 'none' );
            }

            return $q.reject(rejection);
        }
    };
}]);

app.factory( 'Cart', ['$rootScope', '$window', function($rootScope, $window){
    var cart = {itemsCount: 0,
        totalPrice: 0,

        combos: {
            items: [],
            totalPrice: 0,
            itemCount: 0
        },

        categories: {
            items: [],
            totalPrice: 0,
            itemCount: 0
        }};

    var getHashCodeFromString = function(s){
        return s.split("").reduce(function(a,b){a=((a<<5)-a)+b.charCodeAt(0);return a&a},0);
    };

    var writeToCache = function () {
        $window.localStorage.cart = JSON.stringify( cart );
        $window.localStorage.cartHash = '' + getHashCodeFromString( JSON.stringify( $window.localStorage.cart ));
    };

    return {
        updateCategoriesInCart : function ( categoryList ) {
            cart.categories = {
                items : [],
                totalPrice : 0,
                itemCount : 0
            };

            var hasUpdated = false;

            /**Loop for all categories */
            angular.forEach( categoryList, function( category ){
                var itemCount = 0;
                var totalPrice = 0;
                var cartCategory = { name : category.name, items : []};

                /* Loop for items in category */
                angular.forEach( category.items, function( categoryItem ) {
                    var cartItem = {};

                    if( angular.isDefined( categoryItem.qty ) && categoryItem.finalized ){

                        try{
                            var itemQty = parseInt( categoryItem.qty );
                            var singleItemPrice = categoryItem.price;

                            if( itemQty > 0 ) {
                               itemCount = itemCount + itemQty;

                                cartItem.name = categoryItem.name;
                                cartItem.qty = itemQty;

                                cartItem.price = categoryItem.price;
                                cartItem.totalPrice = itemQty * categoryItem.price;

                                totalPrice = totalPrice + cartItem.totalPrice;
                                cartCategory.items.push( cartItem );
                            }
                        }catch( err ){}
                    }
                });

                if( cartCategory.items.length > 0 ){
                    cart.categories.items.push( cartCategory );
                    cart.categories.itemCount = cart.categories.itemCount + itemCount;

                    cart.categories.totalPrice = cart.categories.totalPrice + totalPrice;
                    hasUpdated = true;
                }
            });

            if( hasUpdated  ) {
                cart.itemsCount = cart.combos.itemCount + cart.categories.itemCount;
                cart.totalPrice = cart.combos.totalPrice + cart.categories.totalPrice;

                $rootScope.$broadcast('cartUpdated');
                writeToCache();
            }
        },
        updateCombosInCart : function( combos ) {

            cart.combos = {
                items : [],
                totalPrice : 0,
                itemCount : 0
            };

            var hasUpdated = false;

            var itemCount = 0;
            var totalPrice = 0;

            angular.forEach( combos, function( combo ){
                if( angular.isDefined( combo.qty ) && combo.finalized ) {
                    var comboObject = { name : combo.name, price : combo.price };

                    var itemQty = parseInt( combo.qty );

                        itemCount = itemCount + itemQty;
                        comboObject.totalPrice = comboObject.price * itemQty;
                        comboObject.qty = itemQty;

                        totalPrice = totalPrice + comboObject.totalPrice;

                        cart.combos.items.push( comboObject );
                    hasUpdated = true;
                }
            });

            cart.combos.totalPrice = totalPrice;
            cart.combos.itemCount = itemCount;

            if( hasUpdated  ) {
                cart.itemsCount = cart.combos.itemCount + cart.categories.itemCount;
                cart.totalPrice = cart.combos.totalPrice + cart.categories.totalPrice;
                $rootScope.$broadcast('cartUpdated');
                writeToCache();
            }
        },
        getCartItemsCount : function () {
            return cart.categories.itemCount + cart.combos.itemCount;
        },
        NetTotalPrice : function () {
            return cart.categories.totalPrice + cart.combos.totalPrice;
        },
        getCart : function () {
            return cart;
        },
        clearCart : function () {
            cart = {itemsCount: 0,
                totalPrice: 0,

                combos: {
                    items: [],
                    totalPrice: 0,
                    itemCount: 0
                },

                categories: {
                    items: [],
                    totalPrice: 0,
                    itemCount: 0
                }};
            $rootScope.$broadcast('cartUpdated');
        }
    }
}]);

app.controller('IndexController', ['$scope', '$location', '$timeout', 'User', '$http', 'Cart', function($scope, $location, $timeout, User, $http, Cart){
    console.log( 'Index Controller' );

    $scope.itemsInCart = 0;
    $scope.totalPrice = 0;

    $scope.cart = Cart.getCart();
    console.log( $scope.cart );

    $scope.user = {};

    $scope.loginDialog = true;
    $scope.loginActiveInModal = true;

    $scope.serverError = false;
    $scope.serverSuccess = false;

    $scope.userContextMenu = false;

    $scope.moveToHome = function(){
        $location.path('/');
    };

    $scope.$on('cartUpdated', function(){
        $scope.itemsInCart = Cart.getCartItemsCount();
        $scope.totalPrice = Cart.getTotalPrice();

        if( $scope.itemsInCart > 0 ) {
            angular.element( document.getElementById('cartItems' )).removeClass('scaleUpAnimation');
            $timeout( function () {
                angular.element( document.getElementById('cartItems' )).addClass('scaleUpAnimation');
            }, 20);
        }

        $scope.cart = Cart.getCart();
        console.log( $scope.cart );
    });

    $scope.isLoginDialog = function() { return $scope.loginDialog };

    $scope.isLoginActiveInModal = function () {
        return $scope.loginActiveInModal;
    };

    $scope.activeLogin = function ($event) {
        $scope.loginActiveInModal = true;

        $event.stopPropagation();
    };

    $scope.activeRegister = function($event){
        $scope.loginActiveInModal = false;

        $event.stopPropagation();
    };

    $scope.stopPropagation = function($event ){
        $event.stopPropagation();
    };

    $scope.handleUserGlyphClick = function () {
        if( User.isUserLoggedIn() ){
            $scope.loginDialog = false;

            $scope.userContextMenu = !$scope.userContextMenu;
        } else {
            $scope.loginDialog = true;
            $scope.loginActiveInModal = true;

            $scope.serverError = false;
            $scope.serverSuccess = false;

            $scope.openDialog();
        }
    };

    $scope.isUserContextMenuOpen = function () {
        return !$scope.loginDialog && $scope.userContextMenu;
    };

    $scope.openDialog = function () {

        var overlayDiv = angular.element( document.getElementById( 'overlay' ));
        overlayDiv.css('visibility', 'visible');
        overlayDiv.addClass('active');

        var modalContent = angular.element( document.getElementById( 'modalContent' ));
        modalContent.addClass( 'active' );

        var loader = angular.element( document.getElementById( 'loader' ));
        loader.css( 'display' , 'none' );
    };

    $scope.closeDialog = function () {
        var overlayDiv = angular.element( document.getElementById( 'overlay' ));
        overlayDiv.css('visibility', 'hidden');
        overlayDiv.removeClass('active');

        var modalContent = angular.element( document.getElementById( 'modalContent' ));
        modalContent.removeClass( 'active' );
    };

    $scope.getUserName = function () {
        return User.getUserName();
    };

    $scope.logoutUser = function ($event) {
        User.logoutUser();
        $scope.userContextMenu = false;
        $event.stopPropagation();
    };

    $scope.register = function () {
        var requestName = 'api/users';

        $http.post( requestName, $scope.user )
            .success( function (data ){
                $scope.serverError = false;
                $scope.serverSuccess = true;

                $scope.serverResponse = 'Registration Successful.';
                $scope.loginActiveInModal = true;
            })
            .error( function( data ){
                $scope.serverSuccess = false;
                $scope.serverError = true;
                $scope.serverResponse= data.msg;
            });
    };

    $scope.login = function () {
        var requestName = 'api/user';

        $http.post( requestName, $scope.user )
            .success( function (data ){
                $scope.serverError = false;
                $scope.serverSuccess = true;

                $scope.serverResponse = 'Login Successful.';

                User.setUserInfo( data.result );
                User.setUserToken( data.token );

                $timeout( function(){$scope.closeDialog()}, 1000);
            })
            .error( function( data ){
                $scope.serverSuccess = false;
                $scope.serverError = true;
                $scope.serverResponse= data.msg;
            });
    };

    $scope.isServerResponseNotPresent = function () {
        return !($scope.serverSuccess || $scope.serverError);
    };

    $scope.isServerSuccess = function () {
        return $scope.serverSuccess;
    };

    $scope.isServerError = function () {
        return $scope.serverError;
    };

    $scope.openCart = function () {
        $location.path('/checkout');
    };

    $scope.getCart = function () {
        $scope.cart = Cart.getCart();
    };

    $scope.myOrders = function () {
        $location.path( '/orders' );
    }
}]);

app.controller('FestiveHomeController', ['$scope', 'AppConstants', 'Combos', 'Categories', '$location', 'Cart', function($scope, AppConstants, Combos, Categories, $location, Cart){
    $scope.heading = AppConstants.heading;

    $scope.combos = Combos.getCombos();
    $scope.categories = Categories.getCategories();

    $scope.showAllCategory = function (){
        $location.path( '/category/all')
    };

    console.log(Cart.getCart());
}]);

app.controller('CategoryListController', ['$scope', '$routeParams', 'Combos', 'Categories', '$http', 'CategoryItems', function( $scope, $routeParams, Combos, Categories, $http, CategoryItems){
    $scope.categories = Categories.getCategories();

    $scope.init = function () {
        angular.forEach($scope.categories, function (category) {
            category.items = CategoryItems.getItemsFor(category.slug);
        });
    };

    $scope.init();

    $scope.$on('categoryItemsUpdated', function () {
        $scope.init();
    });

    console.log( $scope.categories );

    console.log($routeParams);
}]);

app.controller('CheckOutController', ['$scope', 'Cart', '$http', 'User', function( $scope, Cart, $http, User ){

    $scope.cart = Cart.getCart();
    $scope.orderPlace = false;
    $scope.isUserNotAuthenticated = false;
    $scope.isError = false;

    $scope.info = '';

    $scope.isPlaceOrderDisabled = function (){
        if( $scope.cart.itemsCount === 0 ) return true;
        return false;
    }

    $scope.getClassForAlert = function () {
        if( $scope.orderPlace ) { $scope.info = 'Your Order has been placed.';return 'alert-info' };
        if( $scope.isError ) { return 'alert-danger' }

        return 'noHeight';
    };

    $scope.checkout = {};

    $scope.getUserInfo = function () {
        if (User.isUserLoggedIn()) {
            $scope.checkout = {
                name: User.getUserName(),
                email: User.getUserEmail()
            }
            $scope.isUserNotAuthenticated = false;
        } else {
            $scope.isUserNotAuthenticated = true;
        }

    };

    $scope.getUserInfo();

    $scope.$on('updatedUserInfo', function () {
        $scope.getUserInfo();
    });

    $scope.placeOrder = function () {
        var requstName = 'api/orders';

        var payLoad = {
            cart : $scope.cart,
            person : $scope.checkout
        };

        $http.post( requstName, payLoad )
            .success(function( data ){
                $scope.isUserNotAuthenticated = false;
                $scope.orderPlace = true;
                $scope.isError  = false;

                Cart.clearCart();

                $scope.err = {};
            })
            .error( function( data ){
                $scope.isUserNotAuthenticated = false;
                $scope.orderPlace = false;

                if( angular.isUndefined( data.err )) {
                    $scope.isError = true;
                }
                else {
                    $scope.err = data.err;
                    $scope.errMsg = data.errMsg;
                }

                $scope.info = data.msg;
            });
    };


    angular.forEach( $scope.cart.combos.items, function( combo ){
        combo.finalized = true;
    });

    angular.forEach( $scope.cart.categories.items, function( category ){
        angular.forEach( category.items, function( item ){
            item.finalized = true;
        });
    });

    $scope.updateCart = function (){
        Cart.updateCombosInCart( $scope.cart.combos.items );
        Cart.updateCategoriesInCart( $scope.cart.categories.items );
    };

    $scope.$on('updateCartInController', function(){
        $scope.$apply( $scope.updateCart() );
    });

    $scope.$on('cartUpdated', function () {
        $scope.cart = Cart.getCart();

        angular.forEach( $scope.cart.combos.items, function( combo ){
            combo.finalized = true;
        });

        angular.forEach( $scope.cart.categories.items, function( category ){
            angular.forEach( category.items, function( item ){
                item.finalized = true;
            });
        });
    });

    $scope.err = {};
    $scope.errMsg = {};

    $scope.hasError = function ( type ) { return $scope.err[ type ]};
    $scope.getMessage = function( type ) { return $scope.errMsg[ type ] };

    $scope.cart.promo = { code : '' };

    $scope.isPromoCodeValid = function () {
        $http.get( 'api/promo/' + $scope.cart.promo.code )
            .success(function( data ){
                $scope.promoResponse = true;
                $scope.promoSuccess = true;
                $scope.promoError = false;
                $scope.cart.promo = {
                    code : data.result.code,
                    discount : data.result.discount,
                    net : $scope.computeDiscountedPrice( data.result.discount )
                };

                $scope.isPromoCodeApplied = true;
            })
            .error( function( data ) {
                $scope.promoResponse = true;
                $scope.promoError = true;
                $scope.promoSuccess = false;

                $scope.isPromoCodeApplied = false;

                $scope.cart.promo = {
                    code : null,
                    discount : null,
                    net : null
                };
            })
    };

    $scope.computeDiscountedPrice = function( discount ) {
        var reuslt = $scope.cart.totalPrice * ( 1 - (.01 * discount ));

        return reuslt;
    }

}]);

app.controller('OrderListController', ['$scope', '$location', 'User', '$http', function( $scope, $location, User, $http ){
    $scope.orders = [];
    $scope.init = function (){
        if( User.isUserLoggedIn() ) {
            var requestName = 'api/auth/user/orders';

            $http.get(requestName)
                .success(function (data) {
                    $scope.orders = data.result;
                })

                .error(function (data) {
                console.log(data);
            });
        } else {
            $location.path( '/' );
        }
    };

    $scope.getOrderItemString = function ( order ) {
        var string = '';

        angular.forEach( order.cart.combos, function( item ){
            string = string + item.qty + ' - ' + item.name + ' Combo, '
        });

        angular.forEach( order.cart.categories, function( category ) {
           angular.forEach( category.items, function( item ) {
               string = string + item.qty + ' - ' + item.name + ' , '
           })

        });

        return string.substr(0, string.lastIndexOf(','));
    };

    $scope.init();
}]);